package ru.trifonov.tm.api;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.model.Task;

import java.util.List;

public interface ITaskService {
    @SneakyThrows
    void insert(
            @Nullable String projectId, @Nullable String name,
            @Nullable String description, @Nullable String beginDate, @Nullable String endDate
    );

    @SneakyThrows
    void update(
            @Nullable String id, @Nullable String projectId, @Nullable String name,
            @Nullable String description, @Nullable String beginDate, @Nullable String endDate
    );

    List<Task> findAll();

    @NotNull List<Task> findAllByProjectId(@NotNull String projectId);

    Task find(@Nullable String id);

    void delete(@Nullable String id);
}
