package ru.trifonov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.trifonov.tm.api.IProjectService;
import ru.trifonov.tm.api.ITaskService;
import ru.trifonov.tm.model.Project;
import ru.trifonov.tm.model.Task;
import ru.trifonov.tm.repository.ITaskRepository;

import java.util.List;

@Service
public final class TaskService extends AbstractService implements ITaskService {
    @NotNull
    ITaskRepository taskRepository;
    IProjectService projectService;

    @Autowired
    public TaskService(@NotNull final ITaskRepository taskRepository, @NotNull final IProjectService projectService) {
        this.taskRepository = taskRepository;
        this.projectService = projectService;
    }

    @Override
    @SneakyThrows
    public void insert(
            @Nullable final String projectId, @Nullable final String name,
            @Nullable final String description, @Nullable final String beginDate, @Nullable final String endDate
    ) {
        if (projectId == null || projectId.trim().isEmpty()) throw new NullPointerException("Enter correct projectid.");
        if (name == null || name.trim().isEmpty()) throw new NullPointerException("Enter correct name.");
        if (description == null || description.trim().isEmpty()) throw new NullPointerException("Enter correct description.");
        if (beginDate == null) throw new NullPointerException("Enter correct begin date.");
        if (endDate == null) throw new NullPointerException("Enter correct end date.");
        @Nullable final Project project =  projectService.find(projectId);
        if (project == null) throw new NullPointerException("Project not found.");
        @NotNull final Task task = new Task(
                project,
                name,
                description,
                dateFormat.parse(beginDate),
                dateFormat.parse(endDate)
        );
        taskRepository.save(task);
    }

    @Override
    @SneakyThrows
    public void update(
            @Nullable final String id, @Nullable final String projectId, @Nullable final String name,
            @Nullable final String description, @Nullable final String beginDate, @Nullable final String endDate
    ) {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct id.");
        if (projectId == null || projectId.trim().isEmpty()) throw new NullPointerException("Enter correct projectid.");
        if (name == null || name.trim().isEmpty()) throw new NullPointerException("Enter correct name.");
        if (description == null || description.trim().isEmpty()) throw new NullPointerException("Enter correct description.");
        if (beginDate == null) throw new NullPointerException("Enter correct begin date.");
        if (endDate == null) throw new NullPointerException("Enter correct end date.");
        @Nullable final Project project =  projectService.find(projectId);
        if (project == null) throw new NullPointerException("Project not found.");
        @NotNull final Task task = new Task(
                id,
                project,
                name,
                description,
                dateFormat.parse(beginDate),
                dateFormat.parse(endDate)
        );
        taskRepository.save(task);
    }

    @NotNull
    @Override
    public List<Task> findAll() {
        @Nullable List<Task> tasks = taskRepository.findAll();
        if (tasks == null || tasks.isEmpty()) throw new NullPointerException("Tasks not found.");
        return tasks;
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@NotNull final String projectId) {
        if (projectId == null || projectId.trim().isEmpty()) throw new NullPointerException("Enter correct project id.");
        @Nullable List<Task> tasks = taskRepository.findAllByProjectId(projectId);
        if (tasks == null || tasks.isEmpty()) throw new NullPointerException("Tasks not found.");
        return tasks;
    }

    @NotNull
    @Override
    public Task find(@Nullable final String id) {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct id.");
        if (!taskRepository.findById(id).isPresent()) throw new NullPointerException("Task not found.");
        @NotNull Task task = taskRepository.findById(id).get();
        return task;
    }

    @Override
    public void delete(@Nullable final String id) {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct id.");
        taskRepository.deleteById(id);
    }
}
