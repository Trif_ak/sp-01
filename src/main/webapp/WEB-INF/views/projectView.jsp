<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <title>VIEW PROJECT</title>
</head>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a style="font-size: 26px" class="navbar-brand mb-0 h1" href="/">PROJECT_MANAGER</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div style="margin-left: 30px" class="collapse navbar-collapse" id="navbarText">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a style="font-size: 20px" class="nav-link" href="/">HOME</a>
            </li>
            <li class="nav-item">
                <a style="font-size: 20px; margin-left: 10px" class="nav-link" href="/projects">PROJECTS</a>
            </li>
            <li class="nav-item">
                <a style="font-size: 20px; margin-left: 10px" class="nav-link" href="/tasks">TASKS</a>
            </li>
        </ul>
    </div>
</nav>
<table class="table">
    <thead class="thead-dark">
    <tr>
        <th scope="col">NAME</th>
        <th scope="col">DESCRIPTION</th>
        <th scope="col">ID</th>
        <th scope="col">STATUS</th>
        <th scope="col">DATE OF BEGIN</th>
        <th scope="col">DATE OF END</th>
        <th scope="col">DATE OF CREATE</th>
        <th scope="col">EDIT</th>
        <th scope="col">REMOVE</th>
    </tr>
    </thead>
    <tbody>
        <tr>
            <td>${project.name}</td>
            <td>${project.description}</td>
            <td>${project.id}</td>
            <td>${project.status}</td>
            <td>${project.beginDate}</td>
            <td>${project.endDate}</td>
            <td>${project.createDate}</td>
            <td>
                <a href="/project-edit?id=${project.id}">EDIT</a>
            </td>
            <td>
                <a href="/project-delete?id=${project.id}">REMOVE</a>
            </td>
        </tr>
    </tbody>
</table>
<a style="margin-left: 20px; margin-top: 5px" href="/projects" class="btn btn-danger active" role="button" aria-disabled="true">BACK</a>
<a style="margin-left: 10px; margin-top: 5px" href="/project-view" class="btn btn-secondary active" role="button" aria-disabled="true">REFRESH</a>

</body>
</html>